using module .\RoverTools.psm1
<#
.SYNOPSIS
    Downloads metadata for a pet's images on rover.com

.DESCRIPTION
    Downloads and saves Rover's API information for all images uploaded for a pet.
    Using this script is not straightforward as it requires extracting ID and
    authentication info from your browser when logged in to rover.

.PARAMETER PetId
    Rover's unique identifier for the pet you want to download images for.
    This will be a random string, e.g. "NbPyEzRQ"

.PARAMETER CookiesFile
    Path to text file containing values for required cookies. These values
    need to be copied from your browser's dev tools while logger in to rover.
    These identify you to rover.com and allow the script to ask rover for data
    on your behalf.

    Example (these values are all expired):

        SESSION_ID    = 8qvn8zd8fmpv4jpmbealesa4ewr1cj7b
        ROVER_CID     = DU5jvtU2TKjQ-q30-ut8R
        CSRF_TOKEN    = 8mvsU3s8GMCpIh6282DZoxjxewC9tXjjM5n5eqIJlKlpudZgdCMwUxTsis4J7hjh
        CFR_CLEARANCE = akVmK9sdZzmLPxrhwkkBAeih0KvnmwlSnZtYvCVhbgQ-1704935697-0-2-d04165b3.68816552.87403a53-0.2.1704935697
        CF_BM         = EWYi3z5YkuuGP9kH31IvXe8TBLRWWbfHuEBYvsBkDLY-1704935696-1-AdhicWSsztIDUI0YE28R8s2RQ20qogO690f2lW28zHtDtwlBkS+d4FyU/U5B3UT6csN/7eBjZUNMp0Q3mDIgBlE=

.PARAMETER OutFile
    File name / location to save downloaded data.
    Defaults to ".\rover-image-urls-$PetId.json".
 #>
[CmdletBinding()]
param (
    [Parameter(Mandatory)]
    [ValidateNotNullOrWhiteSpace()]
    [string]
    $PetId,

    [Parameter(Mandatory)]
    [ValidateNotNullOrWhiteSpace()]
    [ValidateScript(
        { Test-Path -Path $_ -PathType Leaf },
        ErrorMessage = "can't read file {0}"
    )]
    [string]
    $CookiesFile = "cookies.txt",

    [Parameter()]
    [ValidateNotNullOrWhiteSpace()]
    [string]
    $OutFile = ""
)

# $PetId = "NbPyEzRQ"

if ($OutFile -eq "") {
    $OutFile = ".\rover-image-urls-$PetId.json"
}

Get-Content $CookiesFile | ConvertFrom-StringData -OutVariable Cookies

Invoke-ImageListApiRequest -PetId $PetId `
    -SessionId $Cookies["SESSION_ID"] `
    -RoverCid $Cookies["ROVER_CID"] `
    -CsrfToken $Cookies["CSRF_TOKEN"] `
    -CfClearance $Cookies["CF_CLEARANCE"] `
    -CfBm $Cookies["CF_BM"] |
ConvertTo-Json |
Out-File -FilePath $OutFile -NoClobber
