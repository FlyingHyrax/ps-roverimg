using namespace System

class UploadedImageInfo {
    [Uri]      $FileUrl
    [datetime] $UploadedAt
    [string]   $UploaderName

    # Image download URLs returned from the API look like so:
    #   https://www.rover.com/cf-image-cdn/remote/images/pets/NbPyEzRQ/warsgytbch/original
    # The interesting bits are the last 3 path segments:
    #   NbPyEzRQ/, warsgytbch/, original
    # (Some end with 'original.jpeg' - these still response with webp if that's what's in our Accept header)
    static [hashtable[]] $ComputedProperties = @(
        @{
            MemberName = 'BatchID'
            MemberType = 'ScriptProperty'
            Value = { $this.FileUrl.Segments[-3] -replace '/$' }
        }
        @{
            MemberName = 'FileID'
            MemberType = 'ScriptProperty'
            Value = { $this.FileUrl.Segments[-2] -replace '/$' }
        }
        @{
            MemberName = 'FileName'
            MemberType = 'ScriptProperty'
            Value = { $this.FileID + ".webp" }
        }
    )

    static UploadedImageInfo() {
        $TypeName = [UploadedImageInfo].Name
        foreach ($PropDef in [UploadedImageInfo]::ComputedProperties) {
            Update-TypeData -TypeName $TypeName @PropDef
        }
    }

    UploadedImageInfo([Uri]$FileUrl, [datetime]$UploadedAt, [string]$UploaderName) {
        $this.FileUrl = $FileUrl
        $this.UploadedAt = $UploadedAt
        $this.UploaderName = $UploaderName
    }

    static [UploadedImageInfo] FromApiResponseObject([object]$ImageApiObj) {
        # A good file URL looks like so:
        # https://www.rover.com/cf-image-cdn/remote/images/pets/<PETID>/warsgytbch/original.jpeg
        $api_image_uri = [Uri]::new($ImageApiObj.large_uncropped)
        $no_query_params = $api_image_uri.GetLeftPart([UriPartial]::Path)
        return [UploadedImageInfo]::new(
            [Uri]::new($no_query_params),
            $ImageApiObj.added,
            $ImageApiObj.uploader.name
        )
    }

    static [UploadedImageInfo] FromSavedJsonObject([object]$SavedObject) {
        return [UploadedImageInfo]::new(
            [Uri]::new($SavedObject.FileUrl),
            $SavedObject.UploadedAt,
            $SavedObject.UploaderName
        )
    }

    [string] CreateFilePathWithBatchID() {
        return Join-Path $this.BatchID $this.FileName
    }

    [string] CreateFolderPathWithUploadDate() {
        $localdt = $this.UploadedAt.ToLocalTime()
        $year = $localdt.Year.ToString()
        $month_date = $localdt.ToString("MMdd MMMM d")
        return Join-Path $year $month_date
    }

    [string] CreateFilePathWithUploadDate() {
        return Join-Path $this.CreateFolderPathWithUploadDate() $this.FileName
    }

}

$BROWSER_USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36"

# Actual requests send more cookies than this, but they don't all seem to be necessary.
# I kept anything related to Sessions (login), XSS/CSRF, and CloudFlare site protections
function Get-InitialWebSession([string] $CsrfToken, [string] $SessionId, [string] $RoverCid, [string] $CfBm, [string] $CfClearance) {
    $session = New-Object Microsoft.PowerShell.Commands.WebRequestSession
    $session.UserAgent = $BROWSER_USER_AGENT
    $session.Cookies.Add((New-Object System.Net.Cookie("csrftoken", $CsrfToken, "/", ".rover.com")))
    $session.Cookies.Add((New-Object System.Net.Cookie("sessionid", $SessionId, "/", ".rover.com")))
    $session.Cookies.Add((New-Object System.Net.Cookie("rover_cid", $RoverCid, "/", ".www.rover.com")))
    $session.Cookies.Add((New-Object System.Net.Cookie("__cf_bm", $CfBm, "/", ".rover.com")))
    $session.Cookies.Add((New-Object System.Net.Cookie("cf_clearance", $CfClearance, "/", ".rover.com")))
    return $session
}


# Actual requests also send:
# - "referer"="https://www.rover.com/members/USERID/dogs/PETID/"
# - "x-rover-source"="web"
# But these don't appear to be necessary.
# I also changed the 'Accept' header to remove brotli compression (br)
function Get-ApiRequestHeaders ([string] $Path, [string] $CsrfToken, [string] $Method = 'GET') {
    return @{
        "authority"="www.rover.com"
        "method"=$Method
        #"path"="/api/v7/pets/PETID/images/"
        "path"=$Path
        "scheme"="https"
        "accept"="application/json, text/plain, */*"
        # "accept-encoding"="gzip, deflate, br"
        "accept-encoding"="gzip, deflate"
        "accept-language"="en-US,en;q=0.9"
        "dnt"="1"
        "x-csrftoken"=$CsrfToken
        # "referer"="https://www.rover.com/members/USERID/dogs/PETID/"
        # "x-rover-source"="web"
    }
}

function Get-CdnRequestHeaders () {
    return @{
        "accept"="image/webp,image/apng,*/*"
        "accept-encoding"="gzip, deflate"
        "accept-language"="en-US,en;q=0.9"
    }
}

# Create a URL for requesting JSON about uploaded images for a specific pet ID.
# Results are paginated and requesting this URL will only return the first page.
# A 'next' field in the response content is the URL for the next page of data.
function Get-InitialImageApiUri([string] $petId) {
    $builder = [UriBuilder]::new("https://www.rover.com/")
    $builder.Path = "/api/v7/pets/$petId/images/"
    return $builder.Uri
}

# I think it would be great to automatically extract the right cookies, so you could just
# log in to the site in your browser, then tell the script what browser you use and it
# will go read the cookies. it's possible, doing it looks like this:
# https://github.com/techthoughts2/ApertaCookie/tree/main/src
# the PAINS are:
# - You have to have a SQLite driver, meaning bare minimum a System.Data.SQLite DLL or
#   installing and importing the PSSQLite module more ergonomically,
# - Chromium derivatives encrypt their cookie values. You have to read the key from a JSON
#   file, decode it from base64, then ask windows to decrypt it for you (it's tied to the
#   current user session). Then you have to AES-decrypt any values.
# So... not right now.

function Invoke-ImageListApiRequest {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [string]
        $PetId,

        # Value of the 'sessionid' cookie for rover.com
        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [string]
        $SessionId,

        # Value of the 'rover_cid' cookie for rover.com
        [Parameter()]
        [string]
        $RoverCid = "",

        # Value of the 'csrftoken' cookie for rover.com
        [Parameter(Mandatory)]
        [string]
        $CsrfToken,

        # Value of the 'cf_clearance' cookie for rover.com
        [Parameter()]
        [string]
        $CfClearance = "",

        # Value of the '__cf_bm' cookie for rover.com
        [Parameter()]
        [string]
        $CfBm = ""
    )

    begin {
        $request_url = Get-InitialImageApiUri -petId $PetId
        $request_session = Get-InitialWebSession -SessionId $SessionId -RoverCid $RoverCid -CsrfToken $CsrfToken -CfClearance $CfClearance -CfBm $CfBm
    }

    process {
        while ($null -ne $request_url) {
            $headers = Get-ApiRequestHeaders -Path $request_url.AbsolutePath -CsrfToken $CsrfToken
            $response = Invoke-WebRequest `
                -Method Get -Uri  $request_url `
                -UserAgent $BROWSER_USER_AGENT `
                -WebSession $request_session `
                -Headers $headers
            $response_json = $response | Select-Object -ExpandProperty Content | ConvertFrom-Json

            foreach ($image_attrs in $response_json.results) {
                [UploadedImageInfo]::FromApiResponseObject($image_attrs) | Write-Output
            }

            if ($response_json.next -is [string]) {
                $request_url = [Uri]::new($response_json.next)
            } else {
                $request_url = $null
            }
        }
    }
}

function DownloadImage ([Uri]$ImageUrl, [string]$OutFile, [hashtable[string]]$Headers = $null) {
    if ($null -eq $Headers) {
        $Headers = Get-CdnRequestHeaders
    }
    Write-Debug -Message "Downloading: $ImageUrl"
    Invoke-WebRequest `
        -Method Get -Uri $ImageUrl -OutFile $OutFile `
        -UserAgent $BROWSER_USER_AGENT `
        -Headers $Headers `
        -TimeoutSec 10 -RetryIntervalSec 5 -MaximumRetryCount 3
}

 function Invoke-ImageDownloadRequest {
    [CmdletBinding()]
    param (
        # Object describing images to download
        [Parameter(Mandatory, ValueFromPipeline)]
        [ValidateNotNullOrEmpty()]
        [UploadedImageInfo[]]
        $ImageInfo,

        # Folder to save the image file into
        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string]
        $OutFolder = ".",

        # Set how to group image files inside the output folder
        [Parameter()]
        [ValidateSet("Batch", "Date")]
        [string]
        $GroupBy = "Batch",

        # Time to pause between downloads, in milliseconds
        [Parameter(DontShow)]
        [ValidateRange("Positive")]
        [int]
        $Sleep = 250
    )

    begin {
        $cdn_request_headers = Get-CdnRequestHeaders
        $total_items = 0

        function CreateFolder ([string]$Path) {
            $exists = Test-Path -Path $Path -PathType Container
            if (-not $exists) {
                Write-Debug -Message "Creating folder: $Path"
                New-Item -Path $Path -ItemType Directory
            }
        }

        function PathForDate ([string]$BasePath, [UploadedImageInfo]$Img) {
            return Join-Path $BasePath $Img.CreateFilePathWithUploadDate()
        }

        function PathForBatch ([string]$BasePath, [UploadedImageInfo]$Img) {
            return Join-Path $BasePath $Img.CreateFilePathWithBatchID()
        }

        function PathForImg ([string]$BasePath, [UploadedImageInfo]$Img) {
            switch ($GroupBy) {
                "Batch" {
                    PathForBatch $BasePath $Img ; break
                }
                "Date" {
                    PathForDate $BasePath $Img ; break
                }
                default {
                    throw [Error]::new("Unrecognized grouping option ${GroupBy}")
                }
            }
        }
    }

    process {
        foreach ($img in $ImageInfo) {
            Write-Progress -Activity "Downloading images" -Status $img.FileName -CurrentOperation $img.FileUrl -PercentComplete -1 -SecondsRemaining -1
            $out_file = PathForImg $OutFolder $img
            $out_dir = [IO.Path]::GetDirectoryName($out_file)
            CreateFolder $out_dir
            DownloadImage $img.FileUrl $out_file $cdn_request_headers
            $total_items = $total_items + 1
            Start-Sleep -Milliseconds $Sleep
        }
    }

    end {
        Write-Debug "Downloaded $total_items images into $OutFolder"
        Write-Progress -Completed
    }
 }
