using namespace System
using module '.\RoverTools.psm1'
<#
.SYNOPSIS
    Organize and tag pet photos downloaded from rover.com's image API
.DESCRIPTION
    Requires ExifTool: <https://exiftool.org>

    Performs the following operations:
    1. Copies photos into a new folder structure organized by upload date.
       Photos uploaded on the same day are placed in the same folder.
    2. Adds EXIF metadata tags to the photos for the name of the uploader
       and the upload date
    3. Fixes incorrect file extensions if the actual file type doesn't match

    The 'Path' argument is meant to be used with a pipeline to bulk-process photos,
    for example:

        Get-ChildItem -Path '.\downloaded-photos' -Recurse -Depth 2 -File |
        .\OrganizeAndTag.ps1 -OutputFolder '.\organized-photos' -MetadataFile '.\images.json'

.EXAMPLE
    Get-ChildItem -Path '.\downloaded-photos' -Recurse -Depth 2 -File |
        .\OrganizeAndTag.ps1 -OutputFolder '.\organized-photos' -MetadataFile '.\images.json'
#>
[CmdletBinding(SupportsShouldProcess)]
param (
    [Parameter(Mandatory, ValueFromPipeline)]
    [ValidateNotNullOrEmpty()]
    [string[]]
    $Path,

    [Parameter()]
    [ValidateNotNullOrEmpty()]
    [string]
    $OutputFolder = ".",

    [Parameter()]
    [ValidateNotNullOrEmpty()]
    [string]
    $MetadataFile = ".\images.json"
)

begin {
    function Get-ImageFileType([string]$Path) {
        $data = ExifTool.exe -json -File:FileType -File:MIMEType -File:FileTypeExtension $Path | ConvertFrom-Json
        $data | Format-List | Out-String | Write-Debug
        return $data
    }

    function New-FolderIfNotExists([string]$Path) {
        $ok = Test-Path -Path $Path -PathType Container
        if (-not $ok) {
            Write-Debug -Message "Creating directory $Path"
            New-Item -Path $Path -ItemType Directory -WhatIf:$WhatIfPreference | Out-Null
        }
    }

    function Get-MetadataKey {
        [CmdletBinding()]
        param (
            [Parameter(Mandatory, ParameterSetName="FilePath")]
            [string]
            $FilePath,
            [Parameter(Mandatory, ParameterSetName="ImageMeta")]
            [UploadedImageInfo]
            $ImageMeta
        )

        if ($FilePath) {
            $fid = [IO.Path]::GetFileNameWithoutExtension($FilePath)
            $bid = [IO.Path]::GetFileName([IO.Path]::GetDirectoryName($FilePath))
        } elseif ($ImageMeta) {
            $fid = $ImageMeta.FileID
            $bid = $ImageMeta.BatchID
        }
        return $bid + '/' + $fid
    }

    function Get-MetadataFileMap([string]$Path) {
        $meta_by_id = @{}

        Get-Content -Raw -Path $Path |
        ConvertFrom-Json |
        ForEach-Object {
            $meta = [UploadedImageInfo]::FromSavedJsonObject($_)
            $key = Get-MetadataKey -ImageMeta $meta
            $meta_by_id[$key] = $meta
        }

        return $meta_by_id
    }

    function Get-ExifToolTagOpts([UploadedImageInfo]$ImageMeta) {
        $upload_time_local = $ImageMeta.UploadedAt.ToLocalTime()

        $uploaded_dt = $upload_time_local.ToString('yyyy-MM-dd HH:mm:ss')
        $date_time_tags = @(
            "-IFD0:ModifyDate='$uploaded_dt'"          # 'DateTime'
            "-ExifIFD:DateTimeOriginal='$uploaded_dt'" # 'DateTimeCaptured'
            "-ExifIFD:CreateDate='$uploaded_dt'"       # 'DateTimeDigitized'
        )

        $person = $ImageMeta.UploaderName
        $author_tags = @(
            "-IFD0:Artist='$person'"
            "-ExifIFD:OwnerName='$person'"
            "-ExifIFD:Photographer='$person'"
        )

        $long_date = $upload_time_local.ToString("D")
        $long_time = $upload_time_local.ToString("T")
        $long_date_time = $upload_time_local.ToString("F")
        $key = Get-MetadataKey -ImageMeta $ImageMeta
        $description = "$key uploaded by $person on $long_date at $long_time"
        $description_tags = @(
            "-Title='$long_date_time'"
            "-Comment='$description'"
            "-ExifIFD:UserComment='$description'"
            "-IFD0:ImageDescription='$description'"
            "-ExifIFD:ImageUniqueID='$key'"
        )

        return $date_time_tags + $author_tags + $description_tags
    }

    function Write-ProgressForFile([string]$Path, [int]$N, [int]$T) {
        Write-Progress -Id 0 -Activity "Processing image files" `
            -Status "$N/$T"   `
            -CurrentOperation $Path `
            -PercentComplete (($N / $T) * 100)
    }

    $image_metadata = Get-MetadataFileMap -Path $MetadataFile
    Write-Verbose -Message "Loaded $($image_metadata.Count) image metadata entries from $MetadataFile"

    New-FolderIfNotExists -Path $OutputFolder

    $files_to_process = @()
}

process {
    foreach ($file in $Path) {
        $key = Get-MetadataKey -FilePath $file
        $meta = $image_metadata[$key]
        if (-not $meta) {
            Write-Warning -Message "Missing metadata for $file ($key); skipping"
            continue
        } else {
            $obj = [PSCustomObject]@{
                Src = $file
                Meta = $meta
            }
            $files_to_process += $obj
        }
    }
}

end {
    $total = $files_to_process.Count
    $file_n = 0
    $PSStyle.Progress.View = "Classic"
    foreach ($file in $files_to_process) {
        $file_n++
        Write-ProgressForFile -Path $file.Src -N $file_n -T $total

        $out_folder_rel = $file.Meta.CreateFolderPathWithUploadDate()
        $out_folder = Join-Path $OutputFolder $out_folder_rel
        New-FolderIfNotExists -Path $out_folder

        $ext_for_type = Get-ImageFileType -Path $file.Src | Select-Object -ExpandProperty FileTypeExtension
        Write-Debug "ext_for_type = $ext_for_type"
        $out_file_name = [IO.Path]::ChangeExtension([IO.Path]::GetFileName($file.Src), $ext_for_type)
        $out_path = Join-Path $out_folder $out_file_name
        Write-Debug -Message "src: $($file.Src) dst: $out_path"

        $exiftool_tags = @(Get-ExifToolTagOpts -ImageMeta $file.Meta)
        if ($PSCmdlet.ShouldProcess("ExifTool.exe -out $out_path ... $($file.Src)", "exiftool", $out_path)) {
            ExifTool.exe -quiet -out $out_path $exiftool_tags $file.Src
        }
    }
}

# $files_to_process = Get-ChildItem -Path $InputFolder -Recurse -Depth 2 -File -Exclude '*_original'
