using module '.\RoverTools.psm1'

[CmdletBinding()]
param (
    [Parameter(Mandatory, Position=0)]
    [ValidateNotNullOrWhiteSpace()]
    [string]
    $UrisFile,

    # Parameter help description
    [Parameter(Position=1)]
    [ValidateNotNullOrWhiteSpace()]
    [string]
    $OutputFolder = "."
)

New-Item -Path $OutputFolder -PathType Container -Force

Get-Content -Path $UrisFile -Raw |
ConvertFrom-Json |
ForEach-Object {
    return [UploadedImageInfo]::FromSavedJsonObject($_)
} |
Invoke-ImageDownloadRequest -OutFolder $OutputFolder -GroupBy Batch

# Sadly the -Parallel option is a real PITA if you want to, say, use functions.
# https://stackoverflow.com/a/61273544
# Otherwise this seems like it might be nice to parallelize.
