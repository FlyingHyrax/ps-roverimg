# PSRoverImg

PowerShell module and scripts to bulk download pet photos from Rover.com

Apparently Rover used to have a way to export uploaded photos but no longer has this feature in their web interface. I created these PowerShell scripts to download all the photos of one of my pets after they passed away. The scripts are not straightforward to use and rely on non-public web APIs that could change and break. But maybe it can help someone else get started anyway. (Or one day maybe I clean this up into a proper tool?)

## How to Use

Some _very rough_ directions that assume you already know how to open a terminal and run PowerShell scripts.

### Download Image Metadata

The first step is to get a list of all the images to download. This requires authenticating to rover. This is hard-ish to automate so I used client cookies from my browser.

1. Open your web browser, go to rover.com, and log in.
2. Navigate to the photos page for your pet. In your browser's URL bar the URL should look a bit like this:
   ```
   https://www.rover.com/members/<USERID>/dogs/<PETID>/
   ```
   The `<USERID>` and `<PETID>` parts will have _random letters_ instead - the text in the `<PETID>` space are rover.com's internal unique ID for your pet. _Write this down_.
3. Press `F12` on your keyboard to open your browser dev tools. Find stored cookies in this panel. In Chrome, this is in the "Application" tab, "Storage" section of the left sidebar, "Cookies" item. Select rover.com under "Cookies" in the sidebar.
4. Open `cookies.txt` in Notepad. The text on the left side of the '=' on each line is the name of a cookie. For each one:
    1. Delete any text after / to-the-right-of the '=' sign
    2. Look in the cookies table in your browser for a row with the same 'Name' as a name in `cookies.txt` (but probably in lowercase).
    3. Select that row in the table, select the cookie value, and copy it.
    4. In `cookies.txt` paste the cookie value after the '=' for the matching name.
  Do this at least for the session_id, rover_cid, and csrf_token. The "CF" cookies can maybe be left blank.
5. Run the script `Request-ImageUris`:
   ```
   PS> .\Request-ImageUris.psq -PetId <YOUR PET ID>
   ```
   You can optionally set `-CookiesFile` and `-OutFile` parameters if you want to customize those. This should create 'images.json' (or whatever you set `-OutFile` to) containing a URL, upload time, and uploader name for all the photos for the pet ID you set.

### Download Photos

Next run `Invoke-ImageDownloads.ps1`. This uses the URLs in `images.json` to download the photos. This one does not require authentication, so once you have `images.json` with all the photo URLs in it you can run the download another time (or more than once, if there's a problem) without having to go copy cookies from dev tools.

```
PS> .\Invoke-ImageDownloads.ps1 -UrisFile '.\images.json' -OutputFolder '.\downloaded-photos'
```

- The `-UrisFile` option must match the `-OutFile` that was used for `Request-ImageUris`
- `-OutputFolder` is the location to save downloaded files to. The folder will be created if it doesn't exist, and the script will create additional folders inside it.

Downloading is intentionally rate-limited to avoid abusing Rover's API/CDN, so this may take a while if you have a lot of photos.

When this finishes, the output folder will contain subfolders with random-looking names, and each subfolder will have some WebP image files with random-looking names. The names are internal IDs, and the subfolders group together images that were uploaded at the same time.

### Tag & Organize

The downloaded files don't have any metadata specifying when they were taken, and there's no way to see from the folder names what folder goes with what date. `OrganizeAndTag` uses the data in `images.json` to add EXIF metadata tags for the creation date, author/uploader, and image description. This can help services or apps like Google Photos, iCloud Photos, etc. organize the photos by date and stores who uploaded each photo in the photo itself.

**This requires ExifTool: <https://exiftool.org>**

```
PS> Get-ChildItem -Path `.\downloads` -Recurse -Depth 2 -File | .\OrganizeAndTag.ps1 -OutputFolder `.\organized` -MetadataFile `.\images.json`
```

This will do several things:

1. Organize all the photos by date:
    - Inside `OutputFolder` there will be a folder for each year that has photos
    - Inside each year folder there will be a folder for each month/day that has photos
2. Add date, author, and description metadata as described above
3. Fix any incorrect file extensions - I found that some downloaded images were actually JPEG and not WEBP, and this changes the extension on the JPEGs to match their actual type.
